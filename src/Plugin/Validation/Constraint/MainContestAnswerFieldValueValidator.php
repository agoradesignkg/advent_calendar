<?php

namespace Drupal\advent_calendar\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates that a field is suitable for main contest answer.
 */
class MainContestAnswerFieldValueValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    /** @var \Drupal\advent_calendar\Plugin\Validation\Constraint\MainContestAnswerFieldConstraint $constraint */
    // @todo we must get flexible here... (from items, we could get the parent and ask for the number of doors)
    $expected_length = 24;
    foreach ($items as $item) {
      $value = trim($item->value);
      $value_spaces_trimmed = str_replace(' ', '', $value);
      if (mb_strlen($value_spaces_trimmed) !== $expected_length) {
        $this->context->addViolation($constraint->invalidLength, [
          '%expected' => $expected_length,
          '%actual' => mb_strlen($value_spaces_trimmed),
        ]);
      }
      $invalid_chars = preg_match('/[^A-Z0-9 ]/', $value);
      if ($invalid_chars) {
        $this->context->addViolation($constraint->invalidCharacters);
      }
    }
  }

}
