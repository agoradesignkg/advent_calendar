<?php

namespace Drupal\advent_calendar\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if an entity field is valid for main contest answer.
 *
 * @Constraint(
 *   id = "MainContestAnswerField",
 *   label = @Translation("Main contest answer field constraint", context = "Validation"),
 * )
 */
class MainContestAnswerFieldConstraint extends Constraint {

  public $invalidCharacters = 'The answer contains invalid characters. Only capital letters, numbers and spaces are allowed.';

  public $invalidLength = 'The answer must be exact %expected characters long (excluding whitespace), but is %actual.';

  /**
   * {@inheritdoc}
   */
  public function validatedBy() {
    return '\Drupal\advent_calendar\Plugin\Validation\Constraint\MainContestAnswerFieldValueValidator';
  }

}
