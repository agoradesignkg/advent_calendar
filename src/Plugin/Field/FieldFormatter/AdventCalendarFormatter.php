<?php

namespace Drupal\advent_calendar\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;

/**
 * Plugin implementation of the 'advent_calendar' formatter.
 *
 * @FieldFormatter(
 *   id = "advent_calendar_doors",
 *   label = @Translation("Advent calendar doors"),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 */
class AdventCalendarFormatter extends EntityReferenceEntityFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $view_mode = $this->getSetting('view_mode');
    $doors_rendered = [];

    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    /** @var \Drupal\advent_calendar\Entity\AdventCalendarDoorInterface $entity */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      $view_builder = $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId());
      $doors_rendered[$entity->getPosition()] = $view_builder->view($entity, $view_mode);
    }

    if (empty($doors_rendered)) {
      return [];
    }
    ksort($doors_rendered);

    return [
      '#theme' => 'advent_calendar_doors',
      '#doors' => $doors_rendered,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $entity_type = $field_definition->getTargetEntityTypeId();
    $field_name = $field_definition->getName();
    return $entity_type == 'advent_calendar' && $field_name == 'doors';
  }

}
