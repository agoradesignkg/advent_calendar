<?php

namespace Drupal\advent_calendar\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;

/**
 * Defines the current day cache context.
 *
 * Cache context ID: 'current_day'.
 */
class CurrentDayCacheContext implements CacheContextInterface {

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Current day');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    // @todo DI ftw...
    /** @var \Drupal\advent_calendar\AdventCalendarTimeServiceInterface $time_service */
    $time_service = \Drupal::service('advent_calendar.time_service');
    $date = $time_service->getCurrentDate();
    $xxx = $date->format('d-m-Y');
    return $date->format('d-m-Y');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
