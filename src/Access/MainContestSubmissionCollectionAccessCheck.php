<?php

namespace Drupal\advent_calendar\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines an access checker for the main contest submission collection.
 */
class MainContestSubmissionCollectionAccessCheck implements AccessInterface {

  /**
   * Checks access to the main contest submission collection.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    /** @var \Drupal\advent_calendar\Entity\AdventCalendarInterface $calendar */
    $calendar = $route_match->getParameter('advent_calendar');
    if (!$calendar) {
      return AccessResult::forbidden();
    }
    if (!$calendar->hasMainContest()) {
      return AccessResult::forbidden()->addCacheableDependency($calendar);
    }

    // @todo instead of the overview permission, add a dedicated one for viewing the submissions.
    $permissions = [
      'administer advent_calendar',
      'access advent_calendar overview',
    ];

    return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');
  }

}
