<?php

namespace Drupal\advent_calendar\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Generic access control for advent calendar door entities.
 *
 * @see \Drupal\advent_calendar\Entity\AdventCalendarDoorInterface
 */
class AdventCalendarDoorAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\advent_calendar\Entity\AdventCalendarDoorInterface $entity */
    $result = parent::checkAccess($entity, $operation, $account);

    if ($result->isNeutral()) {
      switch ($operation) {
        case 'update':
          $result = AccessResult::allowedIfHasPermission($account, 'update advent_calendar_door')->addCacheableDependency($entity);
          break;

        case 'view':
        case 'view label':
          $result = AccessResult::allowedIf($entity->getCalendar()->isPublished())
            ->addCacheableDependency($entity->getCalendar());
          break;
      }
    }

    return $result;
  }

}
