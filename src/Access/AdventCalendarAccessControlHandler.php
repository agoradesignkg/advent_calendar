<?php

namespace Drupal\advent_calendar\Access;

use Drupal\entity\EntityAccessControlHandler;

/**
 * Generic access control for advent calendar entities.
 *
 * @see \Drupal\advent_calendar\Entity\AdventCalendarInterface
 * @todo implement
 */
class AdventCalendarAccessControlHandler extends EntityAccessControlHandler {

}
