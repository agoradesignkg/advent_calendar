<?php

namespace Drupal\advent_calendar\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Generic access control for contest submission entities.
 *
 * @see \Drupal\advent_calendar\Entity\SubmissionInterface
 * @todo implement
 */
class ContestSubmissionAccessControlHandler extends EntityAccessControlHandler {

}
