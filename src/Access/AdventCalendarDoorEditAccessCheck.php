<?php

namespace Drupal\advent_calendar\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines an access checker for the advent calendar edit route.
 */
class AdventCalendarDoorEditAccessCheck implements AccessInterface {

  /**
   * Checks access to the advent calendar door edit.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    /** @var \Drupal\advent_calendar\Entity\AdventCalendarInterface $calendar */
    $calendar = $route_match->getParameter('advent_calendar');
    if (!$calendar) {
      return AccessResult::forbidden();
    }

    /** @var \Drupal\advent_calendar\Entity\AdventCalendarDoorInterface $door */
    $door = $route_match->getParameter('advent_calendar_door');
    if (!$door) {
      return AccessResult::forbidden();
    }

    if ($door->getCalendarId() != $calendar->id()) {
      return AccessResult::forbidden()->addCacheableDependency($door);
    }

    return $door->access('update', $account, TRUE);
  }

}
