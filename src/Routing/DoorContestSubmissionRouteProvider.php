<?php

namespace Drupal\advent_calendar\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\Routing\DefaultHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Defines the route provider for door contest submission entities.
 */
class DoorContestSubmissionRouteProvider extends DefaultHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    $route = new Route($entity_type->getLinkTemplate('collection'));
    $route
      ->addDefaults([
        '_entity_list' => 'door_contest_submission',
//        '_title' => 'Door contest submissions',
      ])
      ->setRequirement('_advent_calendar_door_submission_collection_access', 'TRUE')
      ->setOption('parameters', [
        'advent_calendar' => [
          'type' => 'entity:advent_calendar',
        ],
      ])
      ->setOption('_admin_route', TRUE);

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditFormRoute(EntityTypeInterface $entity_type) {
    $route = parent::getEditFormRoute($entity_type);

    $params = $route->getOption('parameters');
    $params['advent_calendar'] = ['type' => 'entity:advent_calendar'];
    $route->setOption('parameters', $params);
    $route->setOption('_admin_route', TRUE);
    $route->setRequirement('_advent_calendar_door_submission_edit_access', 'TRUE');
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDeleteFormRoute(EntityTypeInterface $entity_type) {
    $route = parent::getDeleteFormRoute($entity_type);

    $params = $route->getOption('parameters');
    $params['advent_calendar'] = ['type' => 'entity:advent_calendar'];
    $route->setOption('parameters', $params);
    $route->setOption('_admin_route', TRUE);
    $route->setRequirement('_advent_calendar_door_submission_delete_access', 'TRUE');
    return $route;
  }

}
