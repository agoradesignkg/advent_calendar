<?php

namespace Drupal\advent_calendar\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\Routing\DefaultHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Defines the route provider for advent calendar door entities.
 */
class AdventCalendarDoorRouteProvider extends DefaultHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    $route = new Route($entity_type->getLinkTemplate('collection'));
    $route
      ->addDefaults([
        '_entity_list' => 'advent_calendar_door',
        '_title' => 'Doors',
      ])
      ->setRequirement('_advent_calendar_door_collection_access', 'TRUE')
      ->setOption('parameters', [
        'advent_calendar' => [
          'type' => 'entity:advent_calendar',
        ],
      ])
      ->setOption('_admin_route', TRUE);

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditFormRoute(EntityTypeInterface $entity_type) {
    $route = parent::getEditFormRoute($entity_type);

    $params = $route->getOption('parameters');
    $params['advent_calendar'] = ['type' => 'entity:advent_calendar'];
    $route->setOption('parameters', $params);
    $route->setOption('_admin_route', TRUE);
    $route->setRequirement('_advent_calendar_door_edit_access', 'TRUE');
    return $route;
  }

}
