<?php

namespace Drupal\advent_calendar;

/**
 * Defines the advent calendar time service.
 */
interface AdventCalendarTimeServiceInterface {

  /**
   * Get the current date - allows setting fake dates for debugging.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   The current date. This wrapper function enables us the possibility to
   *   fake the current date and test and debug the application.
   */
  public function getCurrentDate();

}
