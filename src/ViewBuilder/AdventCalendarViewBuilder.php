<?php

namespace Drupal\advent_calendar\ViewBuilder;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Defines the advent calendar view builder.
 */
class AdventCalendarViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function alterBuild(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    /** @var \Drupal\advent_calendar\Entity\AdventCalendarInterface $entity */

    $disable_when_active = [
      'before_start_text',
      'after_end_text',
    ];
    $disable_when_future = [
      'doors',
      'after_end_text',
      'introduction',
    ];
    $disable_when_past = [
      'before_start_text',
      'doors',
      'introduction',
    ];

    if ($entity->isActive()) {
      foreach ($disable_when_active as $field_name) {
        if (!empty($build[$field_name])) {
          $build[$field_name]['#access'] = FALSE;
        }
      }

      if ($entity->hasMainContest()) {
        /** @var \Drupal\advent_calendar\MainContestSubmissionServiceInterface $main_contest_submission_service */
        $main_contest_submission_service = \Drupal::service('advent_calendar.main_contest_submission_service');
        if (!$main_contest_submission_service->checkAlreadySubmittedInSameSession($entity->id())) {
          $submission = $main_contest_submission_service->createSubmission($entity->id());
          /** @var \Drupal\Core\Entity\EntityFormBuilderInterface $entity_form_builder */
          $entity_form_builder = \Drupal::service('entity.form_builder');
          $build['main_contest'] = $entity_form_builder->getForm($submission);
          $build['main_contest']['#weight'] = 99;
          $answer = $entity->getMainContestAnswer();
          $answer_words = explode(' ', $answer);
          $answer_structure = [];
          foreach ($answer_words as $word) {
            $answer_structure[] = mb_strlen($word);
          }
          $build['main_contest']['#attributes']['data-answer-structure'] = '[' . implode(',', $answer_structure) . ']';
        }
        else {
          $build['main_contest'] = [
            '#markup' => '<p class="main-contest-already-participated">' . $this->t('You have already participated in our raffle!') . '</p>',
            '#weight' => 99,
          ];
        }
      }
    }
    elseif ($entity->isFuture()) {
      foreach ($disable_when_future as $field_name) {
        if (!empty($build[$field_name])) {
          $build[$field_name]['#access'] = FALSE;
        }
      }
    }
    else {
      foreach ($disable_when_past as $field_name) {
        if (!empty($build[$field_name])) {
          $build[$field_name]['#access'] = FALSE;
        }
      }
    }
  }

}
