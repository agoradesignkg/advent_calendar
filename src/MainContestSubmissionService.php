<?php

namespace Drupal\advent_calendar;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Default main contest submission service implementation.
 */
class MainContestSubmissionService implements MainContestSubmissionServiceInterface {

  /**
   * The main contest submission storage.
   *
   * @var \Drupal\advent_calendar\Storage\MainContestSubmissionStorageInterface
   */
  protected $submissionStorage;

  /**
   * The private temp store object.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $privateTempStore;

  /**
   * Constructs a MainContestSubmissionService instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $private_temp_store_factory
   *   The private temp store factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, PrivateTempStoreFactory $private_temp_store_factory) {
    $this->submissionStorage = $entity_type_manager->getStorage('main_contest_submission');
    $this->privateTempStore = $private_temp_store_factory->get('advent_calendar');
  }

  /**
   * {@inheritdoc}
   */
  public function createSubmission($calendar_id) {
    return $this->submissionStorage->create([
      'calendar_id' => $calendar_id,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function checkAlreadySubmittedInSameSession($calendar_id) {
    $submissions = $this->privateTempStore->get('main_contest_submissions');
    if (empty($submissions)) {
      return FALSE;
    }
    return in_array($calendar_id, $submissions);
  }

  /**
   * {@inheritdoc}
   */
  public function markSubmission($calendar_id) {
    $submissions = $this->privateTempStore->get('main_contest_submissions');
    if (empty($submissions)) {
      $submissions = [];
    }
    $submissions[] = $calendar_id;
    $this->privateTempStore->set('main_contest_submissions', $submissions);
  }

}
