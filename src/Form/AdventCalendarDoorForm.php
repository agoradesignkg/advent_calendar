<?php

namespace Drupal\advent_calendar\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the add/edit form for advent calendar door entities.
 */
class AdventCalendarDoorForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\advent_calendar\Entity\AdventCalendarDoorInterface $door */
    $door = $this->entity;

    $contest_dependent_fields = [
      'question',
      'option1',
      'option2',
      'option3',
      'correct_option',
      'prize_description',
      'prize_image',
    ];
    foreach ($contest_dependent_fields as $contest_dependent_field) {
      $form[$contest_dependent_field]['#access'] = $door->getCalendar()->hasDailyContests();
    }

    return $form;
  }

}
