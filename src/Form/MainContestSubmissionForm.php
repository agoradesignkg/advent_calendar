<?php

namespace Drupal\advent_calendar\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the add/edit form for main contest submission entities.
 */
class MainContestSubmissionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\advent_calendar\Entity\MainContestSubmissionInterface $entity */
    $entity = $this->getEntity();

    if ($gtc_url = $entity->getCalendar()->getGtcUrl()) {
      $label = $this->t('I have read the <a href=":url" target="_blank" rel="noopener">GTC</a> and agree with them expressly.', [
        ':url' => $gtc_url->toString(),
      ]);
      $form['gtc'] = [
        '#type' => 'checkbox',
        '#title' => $label,
        '#required' => TRUE,
        '#weight' => 90,
      ];
    }

    if ($privacy_statement_url = $entity->getCalendar()->getPrivacyUrl()) {
      $label = $this->t('Yes, I have read the <a href=":url" target="_blank" rel="noopener">privacy policy</a> and I accept it.', [
        ':url' => $privacy_statement_url->toString(),
      ]);

      $form['privacy'] = [
        '#type' => 'checkbox',
        '#title' => $label,
        '#required' => TRUE,
        '#weight' => 91,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\advent_calendar\Entity\MainContestSubmissionInterface $entity */
    $entity = parent::validateForm($form, $form_state);
    /** @var \Drupal\advent_calendar\Storage\MainContestSubmissionStorageInterface $main_contest_submission_storage */
    $main_contest_submission_storage = $this->entityTypeManager->getStorage('main_contest_submission');

    // @todo soll dieses Verhalten konfigurierbar sein?
    if ($main_contest_submission_storage->exists($entity->getCalendarId(), $entity->getEmail())) {
      $form_state->setError($form['mail'], $this->t('You have already participated with this e-mail address!'));
    }

    // @todo soll dieses Verhalten konfigurierbar sein?
    // Validate length.
    $correct_answer_trimmed = $entity->getCalendar()->getMainContestAnswerTrimmed();
    $given_answer_trimmed = $entity->getAnswerTrimmed();
    if (mb_strlen($given_answer_trimmed) < mb_strlen($correct_answer_trimmed)) {
      // @todo translate!
      $form_state->setError($form['answer'], 'Das eingegebenen Lösungswort ist unvollständig!');
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    /** @var \Drupal\advent_calendar\Entity\MainContestSubmissionInterface $entity */
    $entity = $this->getEntity();

    /** @var \Drupal\advent_calendar\MainContestSubmissionServiceInterface $main_contest_submission_service */
    $main_contest_submission_service = \Drupal::service('advent_calendar.main_contest_submission_service');
    $main_contest_submission_service->markSubmission($entity->getCalendarId());
    $this->messenger()->addStatus($this->t('Thank you very much for participating in our raffle!'));
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function actionsElement(array $form, FormStateInterface $form_state) {
    $element = parent::actionsElement($form, $form_state);
    if (!empty($element['submit'])) {
      $element['submit']['#value'] = $this->t('Submit');
    }
    return $element;
  }

}
