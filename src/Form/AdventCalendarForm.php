<?php

namespace Drupal\advent_calendar\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the add/edit form for advent calendar entities.
 */
class AdventCalendarForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\advent_calendar\Entity\AdventCalendarInterface $calendar */
    $calendar = $this->entity;

    if ($this->operation == 'add') {
      $form['main_contest_answer']['#states'] = [
        'required' => [
          'input[name^="has_main_contest["]' => ['checked' => TRUE],
        ],
        'visible' => [
          'input[name^="has_main_contest["]' => ['checked' => TRUE],
        ],
      ];
      $current_year = (int) gmdate('Y');
      $form['calendar_year'] = [
        '#title' => $this->t('Year'),
        '#type' => 'number',
        '#min' => $current_year,
        '#step' => 1,
        '#size' => 4,
        '#default_value' => $current_year,
        '#required' => TRUE,
        '#weight' => -100,
      ];
      $form['calendar_month'] = [
        '#title' => $this->t('Month'),
        '#type' => 'number',
        '#min' => 1,
        '#max' => 12,
        '#step' => 1,
        '#size' => 2,
        '#default_value' => 12,
        '#required' => TRUE,
        '#weight' => -99,
      ];
      $form['number_days'] = [
        '#title' => $this->t('Number of doors/days'),
        '#type' => 'number',
        '#min' => 1,
        '#max' => 31,
        '#step' => 1,
        '#size' => 2,
        '#default_value' => 24,
        '#required' => TRUE,
        '#weight' => -98,
      ];
    }
    if ($this->operation == 'edit') {
      // Never allow to edit certain fields after creation.
      $lock_fields = ['has_main_contest', 'has_daily_contests'];
      foreach ($lock_fields as $lock_field) {
        $form[$lock_field]['#access'] = FALSE;
      }

      // Only show the main contest answer field, if there's a main contest.
      $form['main_contest_answer']['#access'] = $calendar->hasMainContest();

      // Only allow editing certain fields, if the calendar isn't active yet.
      if (!$calendar->isFuture()) {
        $form['main_contest_answer']['widget'][0]['value']['#attributes']['readonly'] = 'readonly';
        $form['before_start_text']['#access'] = FALSE;
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\advent_calendar\Entity\AdventCalendarInterface $calendar */
    $calendar = $this->getEntity();

    if ($this->operation == 'add') {
      // Create start and end date based on the input calendar year.
      // Note that we're explicitly using UTC timezone, so that never ever the
      // 30th of November gets stored as start date.
      $year = $form_state->getValue('calendar_year');
      $month = $form_state->getValue('calendar_month');
      $start_day = 1;
      $number_days = $form_state->getValue('number_days');
      $calendar->set('number_doors', $number_days);

      $start = DrupalDateTime::createFromArray([
        'year' => $year,
        'month' => $month,
        'day' => $start_day,
      ], 'UTC');
      $start->setTime(0, 0, 0);
      $calendar->setStartTime($start->getTimestamp());

      $end = clone $start;
      $end->add(new \DateInterval(sprintf('P%sD', $number_days - 1)));
      $end->setTime(23, 59, 59);
      $calendar->setEndTime($end->getTimestamp());

      if (!$calendar->hasMainContest()) {
        $calendar->main_contest_answer = '';
      }
    }

    $result = $calendar->save();
    $this->messenger()->addStatus($this->t('The advent calendar %label has been successfully saved.', ['%label' => $calendar->label()]));

    if ($this->operation == 'add') {
      $form_state->setRedirect('entity.advent_calendar_door.collection', ['advent_calendar' => $calendar->id()]);
    }
    else {
      $form_state->setRedirect('entity.advent_calendar.canonical', ['advent_calendar' => $calendar->id()]);
    }
    return $result;
  }

}
