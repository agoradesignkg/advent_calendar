<?php

namespace Drupal\advent_calendar\Form;

use Drupal\Core\Entity\ContentEntityForm;

/**
 * Defines the add/edit form for door contest submission entities.
 * @todo implement
 */
class DoorContestSubmissionForm extends ContentEntityForm {

}
