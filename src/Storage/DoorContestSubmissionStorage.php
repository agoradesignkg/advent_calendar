<?php

namespace Drupal\advent_calendar\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * The default storage for door contest submission entities.
 *
 * @see \Drupal\advent_calendar\Entity\DoorContestSubmissionInterface
 */
class DoorContestSubmissionStorage extends SqlContentEntityStorage implements DoorContestSubmissionStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByCalendarId($calendar_id) {
    $query = $this->getQuery();
    $query->condition('door_id.entity.calendar_id', $calendar_id);
    $query->sort('door_id.entity.day', 'ASC');
    $query->sort('submission_id', 'ASC');
    $result = $query->execute();
    return $result ? $this->loadMultiple($result) : [];
  }

}
