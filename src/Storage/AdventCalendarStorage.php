<?php

namespace Drupal\advent_calendar\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * The default storage for advent calendar entities.
 *
 * @see \Drupal\advent_calendar\Entity\AdventCalendarInterface
 * @todo implement
 */
class AdventCalendarStorage extends SqlContentEntityStorage implements AdventCalendarStorageInterface {

}
