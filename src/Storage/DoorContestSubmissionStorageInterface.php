<?php

namespace Drupal\advent_calendar\Storage;

use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the interface for door contest submission entities.
 *
 * @see \Drupal\advent_calendar\Entity\DoorContestSubmissionInterface
 */
interface DoorContestSubmissionStorageInterface extends EntityStorageInterface {

  /**
   * Loads all submissions for the given calendar ID, ordered by day.
   *
   * @param int $calendar_id
   *   The advent calendar ID.
   *
   * @return \Drupal\advent_calendar\Entity\DoorContestSubmissionInterface[]
   *   The submissions.
   */
  public function loadByCalendarId($calendar_id);

}
