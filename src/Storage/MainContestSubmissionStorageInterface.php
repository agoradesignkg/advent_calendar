<?php

namespace Drupal\advent_calendar\Storage;

use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the interface for main contest submission entities.
 *
 * @see \Drupal\advent_calendar\Entity\MainContestSubmissionInterface
 */
interface MainContestSubmissionStorageInterface extends EntityStorageInterface {

  /**
   * Loads all submissions for the given calendar ID.
   *
   * @param int $calendar_id
   *   The advent calendar ID.
   *
   * @return \Drupal\advent_calendar\Entity\DoorContestSubmissionInterface[]
   *   The submissions.
   */
  public function loadByCalendarId($calendar_id);

  /**
   * Checks, if there's already a submission for the given calendar and e-mail.
   *
   * @param int $calendar_id
   *   The advent calendar ID.
   * @param string $mail
   *   The e-mail address.
   *
   * @return bool
   *   TRUE, if there's already a submission for the given calendar and e-mail.
   *   FALSE otherwise.
   */
  public function exists($calendar_id, $mail);

}
