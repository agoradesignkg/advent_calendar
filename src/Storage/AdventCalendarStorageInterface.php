<?php

namespace Drupal\advent_calendar\Storage;

use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the interface for advent calendar storage classes.
 *
 * @see \Drupal\advent_calendar\Entity\AdventCalendarInterface
 */
interface AdventCalendarStorageInterface extends EntityStorageInterface {

}
