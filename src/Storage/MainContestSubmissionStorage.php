<?php

namespace Drupal\advent_calendar\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * The default storage for main contest submission entities.
 *
 * @see \Drupal\advent_calendar\Entity\MainContestSubmissionInterface
 */
class MainContestSubmissionStorage extends SqlContentEntityStorage implements MainContestSubmissionStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByCalendarId($calendar_id) {
    $query = $this->getQuery();
    $query->condition('calendar_id', $calendar_id);
    $query->sort('submission_id', 'ASC');
    $result = $query->execute();
    return $result ? $this->loadMultiple($result) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function exists($calendar_id, $mail) {
    $query = $this->getQuery();
    $query->condition('calendar_id', $calendar_id);
    $query->condition('mail', $mail);
    return $query->count()->execute() >= 1;
  }

}
