<?php

namespace Drupal\advent_calendar;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Default advent calendar time service implementation.
 */
class AdventCalendarTimeService implements AdventCalendarTimeServiceInterface {

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new AdventCalendarTimeService object.
   *
   * @param \Drupal\Core\Config\ConfigFactory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request.
   */
  public function __construct(ConfigFactory $config_factory, AccountProxyInterface $current_user, TimeInterface $time, RequestStack $request_stack) {
    $this->config = $config_factory->get('advent_calendar.settings');
    $this->currentUser = $current_user;
    $this->time = $time;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentDate() {
    $debug_mode_active = $this->config->get('debug_mode');
    $allow_debug_mode = $this->currentUser->hasPermission('debug calendar');
    if ($debug_mode_active && $allow_debug_mode && $date_string = $this->request->query->get('date')) {
      try {
        $date = DrupalDateTime::createFromFormat('Y-m-d', $date_string);
      }
      catch (\Exception $ex) {
        // No need to do anything here. This is just for debugging purposes.
      }
    }
    if (empty($date)) {
      $date = DrupalDateTime::createFromTimestamp($this->time->getRequestTime());
    }
    return $date;
  }

}
