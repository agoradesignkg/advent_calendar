<?php

namespace Drupal\advent_calendar\ListBuilder;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the advent calendar door list builder.
 */
class AdventCalendarDoorListBuilder extends AdventCalendarDependentListBuilder {

  /**
   * {@inheritdoc}
   */
  public function load() {
    return $this->calendar->getDoors();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['day'] = $this->t('Day');
    $header['position'] = $this->t('Position');
    $header['description'] = $this->t('Description');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\advent_calendar\Entity\AdventCalendarDoorInterface $entity */

    $row['day'] = $entity->getDay();
    $row['position'] = $entity->getPosition();
    $row['description'] = strip_tags($entity->getDescription());

    return $row + parent::buildRow($entity);
  }

}
