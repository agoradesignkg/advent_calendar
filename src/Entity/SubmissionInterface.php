<?php

namespace Drupal\advent_calendar\Entity;

use Drupal\address\AddressInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Defines the base interface for advent calendar submissions.
 */
interface SubmissionInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Get the answer.
   *
   * @return string
   *   The answer.
   */
  public function getAnswer();

  /**
   * Get the trimmed answer.
   *
   * @return string
   *   The trimmed answer.
   */
  public function getAnswerTrimmed();

  /**
   * Gets the first name.
   *
   * @return string
   *   The first name.
   */
  public function getFirstName();

  /**
   * Gets the last name.
   *
   * @return string
   *   The last name.
   */
  public function getLastName();

  /**
   * Gets the salutation raw value.
   *
   * @return string
   *   The salutation raw value.
   */
  public function getSalutation();

  /**
   * Gets the formatted salutation.
   *
   * @return string
   *   The formatted salutation.
   */
  public function getSalutationFormatted();

  /**
   * Sets the salutation.
   *
   * @param string $salutation
   *   The salutation.
   *
   * @return $this
   */
  public function setSalutation($salutation);

  /**
   * Returns the full name of the submitter.
   *
   * @param bool $include_salutation
   *   Whether or not to include the salutation. Defaults to TRUE.
   *
   * @return string
   *   The full name.
   */
  public function getFullName($include_salutation = TRUE);

  /**
   * Gets the address.
   *
   * @return \Drupal\address\AddressInterface|null
   *   The address item.
   */
  public function getAddress();

  /**
   * Sets the address.
   *
   * @param \Drupal\address\AddressInterface $address
   *   The address item.
   *
   * @return $this
   */
  public function setAddress(AddressInterface $address);

  /**
   * Returns whether or not an address is set.
   *
   * @return bool
   *   TRUE, if the contact has an address set, FALSE otherwise. This should
   *   actually never be FALSE.
   */
  public function hasAddress();

  /**
   * Gets the phone number.
   *
   * @return string
   *   The phone number.
   */
  public function getPhone();

  /**
   * Sets the phone number.
   *
   * @param string $phone
   *   The phone number.
   *
   * @return $this
   */
  public function setPhone($phone);

  /**
   * Gets the email.
   *
   * @return string
   *   The email.
   */
  public function getEmail();

  /**
   * Sets the email.
   *
   * @param string $mail
   *   The email.
   *
   * @return $this
   */
  public function setEmail($mail);

  /**
   * Get the creation timestamp.
   *
   * @return int
   *   The creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Set the creation timestamp.
   *
   * @param int $timestamp
   *   The creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

  /**
   * Get whether the answer is correct.
   *
   * @return bool
   *   TRUE, if the answer is correct, FALSE otherwise.
   */
  public function isCorrect();

  /**
   * Get the parent advent calendar.
   *
   * @return \Drupal\advent_calendar\Entity\AdventCalendarInterface
   *   The parent advent calendar.
   */
  public function getCalendar();

  /**
   * Gets the parent advent calendar ID.
   *
   * @return int
   *   The parent advent calendar ID.
   */
  public function getCalendarId();

}
