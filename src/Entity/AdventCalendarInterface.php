<?php

namespace Drupal\advent_calendar\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Url;

/**
 * Defines the advent calendar interface.
 */
interface AdventCalendarInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Get the name.
   *
   * @return string
   *   The name.
   */
  public function getName();

  /**
   * Set the name.
   *
   * @param string $name
   *   The name.
   *
   * @return $this
   */
  public function setName($name);

  /**
   * Get the creation timestamp.
   *
   * @return int
   *   The creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Set the creation timestamp.
   *
   * @param int $timestamp
   *   The creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the start time.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   *   The start time.
   */
  public function getStartTime();

  /**
   * Gets the end time.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   *   The end time.
   */
  public function getEndTime();

  /**
   * Sets the start time.
   *
   * @param int $timestamp
   *   The timestamp.
   *
   * @return $this
   */
  public function setStartTime($timestamp);

  /**
   * Sets the end time.
   *
   * @param int $timestamp
   *   The timestamp.
   *
   * @return $this
   */
  public function setEndTime($timestamp);

  /**
   * Gets whether the calendar has daily contests.
   *
   * @return bool
   *   TRUE, if the calendar is configured to have daily contests, FALSE
   *   otherwise.
   */
  public function hasDailyContests();

  /**
   * Gets whether the calendar has a main contest.
   *
   * @return bool
   *   TRUE, if the calendar is configured to have a main contest, FALSE
   *   otherwise.
   */
  public function hasMainContest();

  /**
   * Gets the total number of doors/days.
   *
   * @return int
   *   The total number of doors/days (24 by default).
   */
  public function getNumberOfDoors();

  /**
   * Gets the doors.
   *
   * @return \Drupal\advent_calendar\Entity\AdventCalendarDoorInterface[]
   *   The referenced doors.
   */
  public function getDoors();

  /**
   * Get the door of a specific day.
   *
   * @param int $day
   *   The day (in other terms, a 1-based index number).
   *
   * @return \Drupal\advent_calendar\Entity\AdventCalendarDoorInterface|null
   *   The referenced door of the specified day.
   */
  public function getDoorByDay($day);

  /**
   * Get the text that should be shown before the start of the calendar.
   *
   * @return string
   *   The text that should be shown before the start of the calendar.
   */
  public function getBeforeStartText();

  /**
   * Set the text that should be shown before the start of the calendar.
   *
   * @param string $text
   *   The text that should be shown before the start of the calendar.
   *
   * @return $this
   */
  public function setBeforeStartText($text);

  /**
   * Get the introduction text.
   *
   * @return string
   *   The introduction text.
   */
  public function getIntroductionText();

  /**
   * Set the introduction text.
   *
   * @param string $text
   *   The introduction text.
   *
   * @return $this
   */
  public function setIntroductionText($text);

  /**
   * Get the text that should be shown after the end of the calendar.
   *
   * @return string
   *   The text that should be shown after the end of the calendar.
   */
  public function getAfterEndText();

  /**
   * Set the text that should be shown after the end of the calendar.
   *
   * @param string $text
   *   The text that should be shown after the end of the calendar.
   *
   * @return $this
   */
  public function setAfterEndText($text);

  /**
   * Get the main contest answer.
   *
   * @return string
   *   The main contest answer.
   */
  public function getMainContestAnswer();

  /**
   * Get the trimmed main contest answer.
   *
   * @return string
   *   The trimmed main contest answer.
   */
  public function getMainContestAnswerTrimmed();

  /**
   * Get the main contest answer letter for the given day.
   *
   * @param int $day
   *   The day (in other terms, a 1-based index number).
   *
   * @return string
   *   The main contest answer letter for the given day.
   */
  public function getMainContestAnswerLetter($day);

  /**
   * Get the url containing the GTC.
   *
   * @return \Drupal\Core\Url|null
   *   The url containing the GTC.
   */
  public function getGtcUrl();

  /**
   * Set the url containing the GTC.
   *
   * @param \Drupal\Core\Url $url
   *   The url containing the GTC.
   *
   * @return $this
   */
  public function setGtcUrl(Url $url);

  /**
   * Get the url containing the privacy agreement.
   *
   * @return \Drupal\Core\Url|null
   *   The url containing the privacy agreement.
   */
  public function getPrivacyUrl();

  /**
   * Set the url containing the privacy agreement.
   *
   * @param \Drupal\Core\Url $url
   *   The url containing the privacy agreement.
   *
   * @return $this
   */
  public function setPrivacyUrl(Url $url);

  /**
   * Returns whether the calendar has not started yet.
   *
   * @return bool
   *   TRUE, if the calendar has not started yet, FALSE otherwise.
   */
  public function isFuture();

  /**
   * Returns whether the calendar is active now (started, but not ended).
   *
   * @return bool
   *   TRUE, if the calendar is active now (started, but not ended), FALSE
   *   otherwise.
   */
  public function isActive();

  /**
   * Returns whether the calendar has already ended.
   *
   * @return bool
   *   TRUE, if the calendar has already ended, FALSE otherwise.
   */
  public function isPast();

  /**
   * Returns the active day number. 0 is returned for inactive calendars.
   *
   * ATTENTION: the current implementation is based strictly on the assumption
   * that the calendar always starts on the first day of a month. So any
   * unconventional advent calendar date ranges won't be supported.
   *
   * @return int
   *   The active day number. 0 is returned for inactive calendars.
   */
  public function getActiveDayNumber();

  /**
   * Gets active door.
   *
   * @return \Drupal\advent_calendar\Entity\AdventCalendarDoorInterface|null
   *   The active door or NULL, if the calendar is inactive.
   */
  public function getActiveDoor();

}
