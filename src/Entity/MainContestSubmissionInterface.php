<?php

namespace Drupal\advent_calendar\Entity;

/**
 * Defines the base interface for advent calendar main contest submissions.
 */
interface MainContestSubmissionInterface extends SubmissionInterface {

}
