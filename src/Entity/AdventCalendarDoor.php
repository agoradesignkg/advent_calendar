<?php

namespace Drupal\advent_calendar\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Defines the advent calendar door entity class.
 *
 * @ContentEntityType(
 *   id = "advent_calendar_door",
 *   label = @Translation("Advent calendar door"),
 *   label_collection = @Translation("Advent calendar doors"),
 *   label_singular = @Translation("advent calendar door"),
 *   label_plural = @Translation("advent calendar doors"),
 *   label_count = @PluralTranslation(
 *     singular = "@count advent calendar door",
 *     plural = "@count advent calendar doors",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "access" = "Drupal\advent_calendar\Access\AdventCalendarDoorAccessControlHandler",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\advent_calendar\ListBuilder\AdventCalendarDoorListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\advent_calendar\Form\AdventCalendarDoorForm",
 *       "edit" = "Drupal\advent_calendar\Form\AdventCalendarDoorForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\advent_calendar\Routing\AdventCalendarDoorRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer advent_calendar",
 *   fieldable = FALSE,
 *   translatable = FALSE,
 *   base_table = "advent_calendar_door",
 *   entity_keys = {
 *     "id" = "door_id",
 *     "label" = "day",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "edit-form" = "/advent-calendar/{advent_calendar}/doors/{advent_calendar_door}/edit",
 *     "collection" ="/advent-calendar/{advent_calendar}/doors"
 *   },
 *   render_cache = FALSE
 * )
 */
class AdventCalendarDoor extends ContentEntityBase implements AdventCalendarDoorInterface {

  /**
   * {@inheritdoc}
   */
  public function getDay() {
    return (int) $this->get('day')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPosition() {
    return (int) $this->get('position')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCalendar() {
    return $this->get('calendar_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getCalendarId() {
    return $this->get('calendar_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->get('question')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setQuestion($question) {
    $this->set('question', $question);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOption1() {
    return $this->get('option1')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOption($option_number) {
    if (!in_array($option_number, [1, 2, 3])) {
      return '';
    }
    $field_name = 'option' . $option_number;
    return $this->get($field_name)->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setOption1($option) {
    $this->set('option1', $option);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOption2() {
    return $this->get('option2')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setOption2($option) {
    $this->set('option2', $option);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOption3() {
    return $this->get('option3')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setOption3($option) {
    $this->set('option3', $option);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCorrectOptionNumber() {
    return $this->get('correct_option')->value;
  }

  /**
   * @inheritDoc
   */
  public function getCorrectOptionText() {
    $correct_option = $this->getCorrectOptionNumber();
    if (empty($correct_option)) {
      return '';
    }
    return $this->getOption($correct_option);
  }

  /**
   * {@inheritdoc}
   */
  public function setCorrectOptionNumber($option) {
    $this->set('correct_option', $option);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getImage() {
    $field = $this->get('image');
    return !$field->isEmpty() ? $field->entity : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setImage(File $image) {
    $this->set('image', $image->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getReadmoreUrl() {
    /** @var \Drupal\link\LinkItemInterface $field */
    $field = $this->get('readmore_url');
    return !$field->isEmpty() ? $field->getUrl() : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setReadmoreUrl(Url $url) {
    $url_string = $url->isExternal() ? $url->toString() : $url->getInternalPath();
    $this->set('readmore_url', $url_string);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isFuture() {
    if ($this->getCalendar()->isPast()) {
      return FALSE;
    }
    if ($this->getCalendar()->isFuture()) {
      return TRUE;
    }
    return $this->getDay() > $this->getCalendar()->getActiveDayNumber();
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    if ($this->getCalendar()->isPast() || $this->getCalendar()->isFuture()) {
      return FALSE;
    }
    return $this->getDay() === $this->getCalendar()->getActiveDayNumber();
  }

  /**
   * {@inheritdoc}
   */
  public function isPast() {
    if ($this->getCalendar()->isPast()) {
      return TRUE;
    }
    if ($this->getCalendar()->isFuture()) {
      return FALSE;
    }
    return $this->getDay() < $this->getCalendar()->getActiveDayNumber();
  }

  /**
   * {@inheritdoc}
   */
  public function getMainContestAnswerLetter() {
    return $this->getCalendar()->getMainContestAnswerLetter($this->getDay());
  }

  /**
   * {@inheritdoc}
   */
  public function toUrl($rel = 'canonical', array $options = []) {
    $url = parent::toUrl($rel, $options);
    if (in_array($rel, ['edit-form', 'collection'])) {
      $url->setRouteParameter('advent_calendar', $this->getCalendarId());
    }
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['day'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Day'))
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 1);

    $fields['position'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Position'))
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 1);

    // The calendar backreference, populated by AdventCalendar::postSave().
    $fields['calendar_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Advent calendar'))
      ->setDescription(t('The parent advent calendar.'))
      ->setSetting('target_type', 'advent_calendar')
      ->setReadOnly(TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Image'))
      ->setRequired(FALSE)
      ->setSetting('file_extensions', 'png gif jpg jpeg')
      ->setSetting('alt_field', TRUE)
      ->setSetting('alt_field_required', FALSE)
      ->setSetting('title_field', TRUE)
      ->setSetting('title_field_required', FALSE)
      ->setDisplayOptions('form', [
        'type' => 'image_image',
        'weight' => 1,
        'settings' => [
          'progress_indicator' => 'throbber',
          'preview_image_style' => 'thumbnail',
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'image',
        'weight' => 1,
        'label' => 'hidden',
        'settings' => [
          'image_link' => '',
          'image_style' => 'door',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['readmore_url'] = BaseFieldDefinition::create('link')
      ->setSetting('title',  DRUPAL_REQUIRED)
      ->setLabel(t('Read more'))
      ->setDisplayOptions('form', [
        'type' => 'link_default',
        'weight' => 2,
      ])
      ->setDisplayOptions('view', [
        'type' => 'link',
        'weight' => 2,
        'label' => 'hidden',
        'settings' => [
          'target' => '_blank',
          'rel' => 'noopener',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['question'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Question'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['option1'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Answer option 1'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['option2'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Answer option 2'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['option3'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Answer option 3'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['correct_option'] = BaseFieldDefinition::create('list_integer')
      ->setLabel(t('Correct option'))
      ->setSetting('allowed_values', [1 => 1, 2 => 2, 3 => 3])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 8,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['prize_description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Prize description'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 9,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['prize_image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Prize image'))
      ->setRequired(FALSE)
      ->setSetting('file_extensions', 'png gif jpg jpeg')
      ->setSetting('alt_field', TRUE)
      ->setSetting('alt_field_required', FALSE)
      ->setSetting('title_field', TRUE)
      ->setSetting('title_field_required', FALSE)
      ->setDisplayOptions('form', [
        'type' => 'image_image',
        'weight' => 10,
        'settings' => [
          'progress_indicator' => 'throbber',
          'preview_image_style' => 'thumbnail',
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'image',
        'weight' => 10,
        'label' => 'hidden',
        'settings' => [
          'image_link' => '',
          'image_style' => 'big',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = ['current_day'];
    return $contexts;
  }

}
