<?php

namespace Drupal\advent_calendar\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the door contest submission entity class.
 *
 * @ContentEntityType(
 *   id = "door_contest_submission",
 *   label = @Translation("Door contest submission"),
 *   label_collection = @Translation("Door contest submissions"),
 *   label_singular = @Translation("door contest submission"),
 *   label_plural = @Translation("door contest submissions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count door contest submission",
 *     plural = "@count door contest submissions",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\advent_calendar\Storage\DoorContestSubmissionStorage",
 *     "access" = "Drupal\advent_calendar\Access\ContestSubmissionAccessControlHandler",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\advent_calendar\ListBuilder\DoorContestSubmissionListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\advent_calendar\Form\DoorContestSubmissionForm",
 *       "add" = "Drupal\advent_calendar\Form\DoorContestSubmissionForm",
 *       "edit" = "Drupal\advent_calendar\Form\DoorContestSubmissionForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\advent_calendar\Routing\DoorContestSubmissionRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer advent_calendar",
 *   fieldable = FALSE,
 *   translatable = FALSE,
 *   base_table = "door_contest_submission",
 *   entity_keys = {
 *     "id" = "submission_id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "edit-form" = "/advent-calendar/{advent_calendar}/submissions/{door_contest_submission}/edit",
 *     "delete-form" = "/advent-calendar/{advent_calendar}/submissions/{door_contest_submission}/delete",
 *     "collection" ="/advent-calendar/{advent_calendar}/submissions"
 *   },
 * )
 */
class DoorContestSubmission extends ContestSubmissionBase implements DoorContestSubmissionInterface {

  /**
   * {@inheritdoc}
   */
  public function getCalendarDoor() {
    return $this->get('door_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getDay() {
    return $this->getCalendarDoor()->getDay();
  }

  /**
   * {@inheritdoc}
   */
  public function getCalendar() {
    return $this->getCalendarDoor()->getCalendar();
  }

  /**
   * {@inheritdoc}
   */
  public function getCalendarId() {
    return $this->getCalendarDoor()->getCalendarId();
  }

  /**
   * {@inheritdoc}
   */
  public function isCorrect() {
    $answer = $this->getAnswer();
    return !empty($answer) && $answer == $this->getCalendarDoor()->getCorrectOptionText();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['door_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Day'))
      ->setSetting('target_type', 'advent_calendar_door')
      ->setReadOnly(TRUE);

    return $fields;
  }

}
