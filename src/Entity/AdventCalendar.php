<?php

namespace Drupal\advent_calendar\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Defines the advent calendar entity class.
 *
 * @ContentEntityType(
 *   id = "advent_calendar",
 *   label = @Translation("Advent calendar"),
 *   label_collection = @Translation("Advent calendars"),
 *   label_singular = @Translation("advent calendar"),
 *   label_plural = @Translation("advent calendars"),
 *   label_count = @PluralTranslation(
 *     singular = "@count advent calendar",
 *     plural = "@count advent calendars",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\advent_calendar\Storage\AdventCalendarStorage",
 *     "access" = "Drupal\advent_calendar\Access\AdventCalendarAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "Drupal\advent_calendar\ViewBuilder\AdventCalendarViewBuilder",
 *     "list_builder" = "Drupal\advent_calendar\ListBuilder\AdventCalendarListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\advent_calendar\Form\AdventCalendarForm",
 *       "add" = "Drupal\advent_calendar\Form\AdventCalendarForm",
 *       "edit" = "Drupal\advent_calendar\Form\AdventCalendarForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer advent_calendar",
 *   fieldable = FALSE,
 *   translatable = FALSE,
 *   base_table = "advent_calendar",
 *   entity_keys = {
 *     "id" = "calendar_id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/advent-calendar/{advent_calendar}",
 *     "add-form" = "/advent-calendar/add",
 *     "edit-form" = "/advent-calendar/{advent_calendar}/edit",
 *     "delete-form" = "/advent-calendar/{advent_calendar}/delete",
 *     "collection" = "/admin/advent-calendar/calendars"
 *   },
 *   render_cache = FALSE
 * )
 */
class AdventCalendar extends ContentEntityBase implements AdventCalendarInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * Internal flag, statically caching the result of the time check.
   *
   * If the calendar has not started yet, the flag is set to 1. If it's actively
   * running at the moment, the flag value will be set to 0. If it's already
   * finished, the value will be -1.
   *
   * The value is populated on the first time calling ::getTimeFlag() during the
   * request.
   *
   * @var int
   */
  protected $timeFlag;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStartTime() {
    return $this->time->value ? $this->time->start_date : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndTime() {
    return $this->time->end_value ? $this->time->end_date : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setStartTime($timestamp) {
    $start_time = gmdate(DateTimeItemInterface::DATE_STORAGE_FORMAT, $timestamp);
    $this->time->value = $start_time;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setEndTime($timestamp) {
    $start_time = gmdate(DateTimeItemInterface::DATE_STORAGE_FORMAT, $timestamp);
    $this->time->end_value = $start_time;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasDailyContests() {
    return (bool) $this->get('has_daily_contests')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function hasMainContest() {
    return (bool) $this->get('has_main_contest')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getNumberOfDoors() {
    return (int) $this->get('number_doors')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getDoors() {
    return $this->get('doors')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function getDoorByDay($day) {
    $delta = $day - 1;
    $item = $this->get('doors')->get($delta);
    return $item ? $item->entity : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getBeforeStartText() {
    return $this->get('before_start_text')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setBeforeStartText($text) {
    $this->set('before_start_text', $text);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getIntroductionText() {
    return $this->get('introduction')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setIntroductionText($text) {
    $this->set('introduction', $text);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAfterEndText() {
    return $this->get('after_end_text')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAfterEndText($text) {
    $this->set('after_end_text', $text);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMainContestAnswer() {
    return $this->hasMainContest() ? $this->get('main_contest_answer')->value : '';
  }

  /**
   * {@inheritdoc}
   */
  public function getMainContestAnswerTrimmed() {
    $answer = $this->getMainContestAnswer();
    return str_replace(' ', '', $answer);
  }

  /**
   * {@inheritdoc}
   */
  public function getMainContestAnswerLetter($day) {
    if (!$this->hasMainContest()) {
      return '';
    }
    $answer = $this->getMainContestAnswerTrimmed();
    $index = $day - 1;
    return mb_substr($answer, $index, 1);
  }

  /**
   * {@inheritdoc}
   */
  public function getGtcUrl() {
    $field = $this->get('gtc_url');
    if ($field->isEmpty()) {
      return NULL;
    }
    /** @var \Drupal\link\LinkItemInterface $item */
    $item = $field->first();
    return $item->getUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function setGtcUrl(Url $url) {
    $url_string = $url->isExternal() ? $url->toString() : $url->getInternalPath();
    $this->set('gtc_url', $url_string);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrivacyUrl() {
    $field = $this->get('privacy_url');
    if ($field->isEmpty()) {
      return NULL;
    }
    /** @var \Drupal\link\LinkItemInterface $item */
    $item = $field->first();
    return $item->getUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function setPrivacyUrl(Url $url) {
    $url_string = $url->isExternal() ? $url->toString() : $url->getInternalPath();
    $this->set('privacy_url', $url_string);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isFuture() {
    return $this->getTimeFlag() === 1;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return $this->getTimeFlag() === 0;
  }

  /**
   * {@inheritdoc}
   */
  public function isPast() {
    return $this->getTimeFlag() === -1;
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveDayNumber() {
    $day = 0;
    if ($this->isActive()) {
      /** @var \Drupal\advent_calendar\AdventCalendarTimeServiceInterface $time_service */
      $time_service = \Drupal::service('advent_calendar.time_service');
      $date = $time_service->getCurrentDate();
      $day = (int) $date->format('d');
    }
    return $day;
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveDoor() {
    $active_day = $this->getActiveDayNumber();
    return $active_day ? $this->getDoorByDay($active_day) : NULL;
  }

  /**
   * Helper function for checking the calendar's state.
   *
   * @return int
   *   See the description for the $timeFlag class variable.
   */
  protected function getTimeFlag() {
    if (is_null($this->timeFlag)) {
      /** @var \Drupal\advent_calendar\AdventCalendarTimeServiceInterface $time_service */
      $time_service = \Drupal::service('advent_calendar.time_service');
      $date = $time_service->getCurrentDate();

      // Again we saw that working with PHP native date intervals is quite hard.
      // Do compare full days, it's much simpler to format the date values to an
      // easily comparable integer value instead.
      $today = (int) $date->format('Ymd');
      $start_time = (int) $this->getStartTime()->format('Ymd');
      $end_time = (int) $this->getEndTime()->format('Ymd');

      if ($today < $start_time) {
        $this->timeFlag = 1;
      }
      elseif ($today > $end_time) {
        $this->timeFlag = -1;
      }
      else {
        $this->timeFlag = 0;
      }
    }
    return $this->timeFlag;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    if ($this->get('doors')->isEmpty()) {
      $door_storage = \Drupal::entityTypeManager()->getStorage('advent_calendar_door');

      $positions = [];
      $number_doors = $this->getNumberOfDoors();
      for ($i = 1; $i <= $number_doors; $i++) {
        $positions[] = $i;
      }
      shuffle($positions);

      $delta = 0;
      do {
        $door = $door_storage->create([
          'day' => $delta + 1,
          'position' => array_pop($positions),
        ]);
        $door->save();
        $this->get('doors')->appendItem($door);
        $delta++;
      }
      while ($delta < $number_doors);
}
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Ensure there's a back-reference on each calendar door.
    foreach ($this->doors as $item) {
      $door = $item->entity;
      if ($door->calendar_id->isEmpty()) {
        $door->calendar_id = $this->id();
        $door->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    // Delete the doors of a deleted calendar.
    $doors = [];
    foreach ($entities as $entity) {
      if (empty($entity->doors)) {
        continue;
      }
      foreach ($entity->doors as $item) {
        $doors[$item->target_id] = $item->entity;
      }
    }
    $door_storage = \Drupal::entityTypeManager()->getStorage('advent_calendar_door');
    $door_storage->delete($doors);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setRequired(TRUE)
      ->setSetting('default_value', '')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['time'] = BaseFieldDefinition::create('daterange')
      ->setLabel(t('Time'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE)
      ->setSettings([
        'datetime_type' => DateTimeItem::DATETIME_TYPE_DATE
      ]);

    $fields['has_daily_contests'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Has daily contests'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 2,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['has_main_contest'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Has main contest'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 3,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['main_contest_answer'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Main contest answer'))
      ->setDescription(t('The answer must be @days characters long, but may include spaces that are not counted.', ['@days' => 24]))
      ->setSetting('default_value', '')
      ->setSetting('max_length', 255)
      ->setDefaultValue('FROHES FEST UND GUTEN RUTSCH')
      ->setConstraints([
        'MainContestAnswerField' => [],
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['before_start_text'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Before start text'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 5,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['introduction'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Introduction text'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 6,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['after_end_text'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('After end text'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 7,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['number_doors'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Number of doors'))
      ->setDefaultValue(24)
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 1);

    $fields['doors'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Doors'))
      ->setSetting('target_type', 'advent_calendar_door')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'advent_calendar_doors',
        'weight' => 8,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['gtc_url'] = BaseFieldDefinition::create('link')
      ->setSetting('title',  DRUPAL_DISABLED)
      ->setLabel(t('GTC'))
      ->setDisplayOptions('form', [
        'type' => 'link_default',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['privacy_url'] = BaseFieldDefinition::create('link')
      ->setSetting('title',  DRUPAL_DISABLED)
      ->setLabel(t('Privacy agreement'))
      ->setDisplayOptions('form', [
        'type' => 'link_default',
        'weight' => 11,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the retailer was last edited.'));

    $fields['status']
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 99,
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = ['current_day'];
    if ($this->hasMainContest()) {
      $contexts[] = 'session';
    }
    return $contexts;
  }

}
