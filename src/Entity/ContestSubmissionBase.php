<?php

namespace Drupal\advent_calendar\Entity;

use Drupal\address\AddressInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the abstract base class for contest submission entities.
 */
abstract class ContestSubmissionBase extends ContentEntityBase implements SubmissionInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getAnswer() {
    return $this->get('answer')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getAnswerTrimmed() {
    $answer = $this->getAnswer();
    return str_replace(' ', '', $answer);
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstName() {
    if (!$this->hasAddress()) {
      return '';
    }
    return $this->getAddress()->getGivenName();
  }

  /**
   * {@inheritdoc}
   */
  public function getLastName() {
    if (!$this->hasAddress()) {
      return '';
    }
    return $this->getAddress()->getFamilyName();
  }

  /**
   * {@inheritdoc}
   */
  public function getSalutation() {
    return $this->get('salutation')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getSalutationFormatted() {
    $raw_value = $this->getSalutation();
    if (empty($raw_value)) {
      return '';
    }
    $allowed_values = advent_calendar_allowed_salutation_values();
    return isset($allowed_values[$raw_value]) ? $allowed_values[$raw_value] : '';
  }

  /**
   * {@inheritdoc}
   */
  public function setSalutation($salutation) {
    $this->set('salutation', $salutation);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFullName($include_salutation = TRUE) {
    $parts = [];
    if ($include_salutation) {
      $salutation = $this->getSalutationFormatted();
      if (!empty($salutation)) {
        $parts[] = $salutation;
      }
    }
    $first_name = $this->getFirstName();
    if (!empty($first_name)) {
      $parts[] = $first_name;
    }
    $last_name = $this->getLastName();
    if (!empty($last_name)) {
      $parts[] = $last_name;
    }
    return !empty($parts) ? implode(' ', $parts) : '';
  }

  /**
   * {@inheritdoc}
   */
  public function getAddress() {
    $address_field = $this->get('address');
    return $address_field->isEmpty() ? NULL : $address_field->first();
  }

  /**
   * {@inheritdoc}
   */
  public function setAddress(AddressInterface $address) {
    $this->set('address', $address);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasAddress() {
    $address = $this->getAddress();
    return !empty($address) && !$address->isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function getPhone() {
    return $this->get('phone')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPhone($phone) {
    $this->get('phone')->value = $phone;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail() {
    return $this->get('mail')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEmail($mail) {
    $this->get('mail')->value = $mail;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->getFullName(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function toUrl($rel = 'canonical', array $options = []) {
    $url = parent::toUrl($rel, $options);
    if (in_array($rel, ['edit-form', 'collection', 'delete-form'])) {
      $url->setRouteParameter('advent_calendar', $this->getCalendarId());
    }
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['answer'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Answer'))
      ->setSetting('max_length', 255)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['salutation'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Salutation'))
      ->setSetting('allowed_values_function', 'advent_calendar_allowed_salutation_values')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 1,
      ])
      ->setDisplayOptions('view', [
        'type' => 'list_default',
        'weight' => 1,
        'label' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['address'] = BaseFieldDefinition::create('address')
      ->setLabel(new TranslatableMarkup('Address'))
      ->setSetting('fields', [
        'administrativeArea' => '',
        'locality' => 'locality',
        'dependentLocality' => '',
        'postalCode' => 'postalCode',
        'sortingCode' => '',
        'addressLine1' => 'addressLine1',
        'addressLine2' => '',
        'organization' => '',
        'givenName' => 'givenName',
        'additionalName' => '',
        'familyName' => 'familyName',
      ])
      ->setSetting('langcode_override', '')
      ->setDisplayOptions('form', [
        'type' => 'address_default',
        'weight' => 2,
        'settings' => [
          'default_country' => 'AT',
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'address_default',
        'weight' => 2,
        'label' => 'hidden',
        'settings' => [],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['phone'] = BaseFieldDefinition::create('telephone')
      ->setLabel(new TranslatableMarkup('Phone'))
      ->setSetting('default_value', '')
      ->setDisplayOptions('form', [
        'type' => 'telephone_default',
        'weight' => 3,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 3,
        'label' => 'inline',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['mail'] = BaseFieldDefinition::create('email')
      ->setLabel(new TranslatableMarkup('Email'))
      ->setSetting('default_value', '')
      ->setDisplayOptions('form', [
        'type' => 'email_default',
        'weight' => 4,
      ])
      ->setDisplayOptions('view', [
        'type' => 'email_mailto',
        'weight' => 4,
        'label' => 'inline',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The time when the workday was created.'))
      ->setTranslatable(FALSE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The time when the workday was last edited.'))
      ->setTranslatable(FALSE);

    return $fields;
  }

}
