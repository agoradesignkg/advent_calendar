<?php

namespace Drupal\advent_calendar\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the main contest submission entity class.
 *
 * @ContentEntityType(
 *   id = "main_contest_submission",
 *   label = @Translation("Main contest submission"),
 *   label_collection = @Translation("Main contest submissions"),
 *   label_singular = @Translation("main contest submission"),
 *   label_plural = @Translation("main contest submissions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count main contest submission",
 *     plural = "@count main contest submissions",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\advent_calendar\Storage\MainContestSubmissionStorage",
 *     "access" = "Drupal\advent_calendar\Access\ContestSubmissionAccessControlHandler",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\advent_calendar\ListBuilder\MainContestSubmissionListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\advent_calendar\Form\MainContestSubmissionForm",
 *       "add" = "Drupal\advent_calendar\Form\MainContestSubmissionForm",
 *       "edit" = "Drupal\advent_calendar\Form\MainContestSubmissionForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\advent_calendar\Routing\MainContestSubmissionRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer advent_calendar",
 *   fieldable = FALSE,
 *   translatable = FALSE,
 *   base_table = "main_contest_submission",
 *   entity_keys = {
 *     "id" = "submission_id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "edit-form" = "/advent-calendar/{advent_calendar}/main-contest-submissions/{main_contest_submission}/edit",
 *     "delete-form" = "/advent-calendar/{advent_calendar}/main-contest-submissions/{main_contest_submission}/delete",
 *     "collection" ="/advent-calendar/{advent_calendar}/main-contest-submissions"
 *   },
 * )
 */
class MainContestSubmission extends ContestSubmissionBase implements MainContestSubmissionInterface {

  /**
   * {@inheritdoc}
   */
  public function getCalendar() {
    return $this->get('calendar_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getCalendarId() {
    return $this->get('calendar_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function isCorrect() {
    $answer = $this->getAnswer();
    return !empty($answer) && $answer == $this->getCalendar()->getMainContestAnswer();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['calendar_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Advent calendar'))
      ->setSetting('target_type', 'advent_calendar')
      ->setReadOnly(TRUE);

    return $fields;
  }

}
