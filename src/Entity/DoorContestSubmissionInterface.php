<?php

namespace Drupal\advent_calendar\Entity;

/**
 * Defines the base interface for advent calendar door contest submissions.
 */
interface DoorContestSubmissionInterface extends SubmissionInterface {

  /**
   * Gets the calendar door.
   *
   * @return \Drupal\advent_calendar\Entity\AdventCalendarDoorInterface
   *   The calendar door.
   */
  public function getCalendarDoor();

  /**
   * Get the assigned day number.
   *
   * @return int
   *   The assigned day number.
   */
  public function getDay();

}
