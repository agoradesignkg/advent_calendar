<?php

namespace Drupal\advent_calendar\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Defines the advent calendar door interface.
 */
interface AdventCalendarDoorInterface extends ContentEntityInterface {

  /**
   * Get the assigned day number.
   *
   * @return int
   *   The assigned day number.
   */
  public function getDay();

  /**
   * Get the assigned position in the calendar.
   *
   * @return int
   *   The assigned position.
   */
  public function getPosition();

  /**
   * Get the parent advent calendar.
   *
   * @return \Drupal\advent_calendar\Entity\AdventCalendarInterface
   *   The parent advent calendar.
   */
  public function getCalendar();

  /**
   * Gets the parent advent calendar ID.
   *
   * @return int
   *   The parent advent calendar ID.
   */
  public function getCalendarId();

  /**
   * Get the question.
   *
   * @return string
   *   The question.
   */
  public function getQuestion();

  /**
   * Set the question.
   *
   * @param string $question
   *   The question.
   *
   * @return $this
   */
  public function setQuestion($question);

  /**
   * Get the answer option 1.
   *
   * @return string
   *   The answer option 1.
   */
  public function getOption1();

  /**
   * Set the answer option 1.
   *
   * @param string $option
   *   The answer option 1.
   *
   * @return $this
   */
  public function setOption1($option);

  /**
   * Get the answer option 2.
   *
   * @return string
   *   The answer option 2.
   */
  public function getOption2();

  /**
   * Set the answer option 2.
   *
   * @param string $option
   *   The answer option 2.
   *
   * @return $this
   */
  public function setOption2($option);

  /**
   * Get the answer option 3.
   *
   * @return string
   *   The answer option 3.
   */
  public function getOption3();

  /**
   * Gets the answer option by its number.
   *
   * @param $option_number
   *   The option number - must be one of: 1, 2, 3.
   *
   * @return string
   *   The option.
   */
  public function getOption($option_number);

  /**
   * Set the answer option 3.
   *
   * @param string $option
   *   The answer option 3.
   *
   * @return $this
   */
  public function setOption3($option);

  /**
   * Get the correct option number.
   *
   * @return int
   *   The correct option number - one of 1, 2 or 3.
   */
  public function getCorrectOptionNumber();

  /**
   * Set the correct option number.
   *
   * @param int $option
   *   The correct option number - one of 1, 2 or 3.
   *
   * @return $this
   */
  public function setCorrectOptionNumber($option);

  /**
   * Get the correct option text.
   *
   * @return string
   *   The correct option text.
   */
  public function getCorrectOptionText();

  /**
   * Get the description text.
   *
   * @return string
   *   The description text.
   */
  public function getDescription();

  /**
   * Set the description text.
   *
   * @param string $description
   *   The description text.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Gets the image file.
   *
   * @return \Drupal\file\Entity\File|null
   *   The image file entity, or null.
   */
  public function getImage();

  /**
   * Sets the image file.
   *
   * @param \Drupal\file\Entity\File $image
   *   The image file entity.
   *
   * @return $this
   */
  public function setImage(File $image);

  /**
   * Get the url containing the readmore url.
   *
   * @return \Drupal\Core\Url|null
   *   The url containing the readmore url.
   */
  public function getReadmoreUrl();

  /**
   * Set the url containing the readmore url.
   *
   * @param \Drupal\Core\Url $url
   *   The url containing the readmore url.
   *
   * @return $this
   */
  public function setReadmoreUrl(Url $url);

  /**
   * Returns whether the door has a future date.
   *
   * @return bool
   *   TRUE, if the door has a future date, FALSE otherwise.
   */
  public function isFuture();

  /**
   * Returns whether the door is the active day.
   *
   * @return bool
   *   TRUE, if the door is active now (today), FALSE otherwise.
   */
  public function isActive();

  /**
   * Returns whether the door is already past.
   *
   * @return bool
   *   TRUE, if the door is already past, FALSE otherwise.
   */
  public function isPast();

  /**
   * Get the main contest answer letter assigned to this door.
   *
   * @return string
   *   The main contest answer letter assigned to this door. An empty string
   *   will be returned, if the calendar is not set to use the main contest.
   */
  public function getMainContestAnswerLetter();

}
