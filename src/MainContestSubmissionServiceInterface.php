<?php

namespace Drupal\advent_calendar;

/**
 * Defines the main contest submission service interface.
 */
interface MainContestSubmissionServiceInterface {

  /**
   * Constructs a new submission entity, without permanently saving it.
   *
   * @param int $calendar_id
   *   The calendar ID, the submission gets attached to.
   *
   * @return \Drupal\advent_calendar\Entity\MainContestSubmissionInterface
   *   A new unsaved main contest submission entity, attached to the specified
   *   calendar.
   */
  public function createSubmission($calendar_id);

  /**
   * Checks whether a calendar already has a submission in the active session.
   *
   * The private temp store is used to mark the submitted main contests in the
   * active session.
   *
   * @param int $calendar_id
   *   The calendar ID to check.
   *
   * @return bool
   *   TRUE, if there's already a submission for the given calendar in the
   *   user session.
   */
  public function checkAlreadySubmittedInSameSession($calendar_id);

  /**
   * Marks the given calendar as submitted in the user session.
   *
   * @param $calendar_id
   *   The calendar ID.
   */
  public function markSubmission($calendar_id);

}
