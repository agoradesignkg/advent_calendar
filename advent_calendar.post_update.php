<?php

/**
 * @file
 * Post update functions of advent_calendar module.
 */

/**
 * Populate the new number_doors field.
 */
function advent_calendar_post_update_1() {
  /** @var \Drupal\advent_calendar\Entity\AdventCalendarInterface $calendars */
  $calendars = \Drupal::entityTypeManager()->getStorage('advent_calendar')->loadMultiple();
  foreach ($calendars as $calendar) {
    $calendar->set('number_doors', 24);
    $calendar->save();
  }
}
